Unser Team besteht aus Simon Henning, Nicklas Düringer, Mirjam Senk.

Wir teilen die verschiedenen Zustände die vor und während des Spieles eintreten können unter uns auf.
Simon H. übernimmt "Spielzug: Schiff versenkt/getroffen/ kein Treffer",
Nicklas D. schreibt den Code für "Schiffe setzen" und
Mirjam S. den für "Spielfeldgröße setzen" und "Spielfeld mit neuem Spielzustand anzeigen".

Die Main() wird von uns drei gemeinsam geschrieben.