Unser Projekt heißt Schiffe versenken.

Es handelt sich hierbei um ein Spiel, bei dem 2 Kontrahenten versuchen die vom Gegner gesetzten Schiffe zu versenken.
Das Spiel beginnt, indem die Kontrahenten ihre ihnen zur Verfügung stehenden Schiffe auf dem Spielfeld platzieren.
Abwechselnd wählen die Kontrahenten Felder auf dem gegnerischen Feld aus. Dabei kann es zu folgenden Spielsituationen kommen.
 Das Feld, welches ausgewählt wurde
  1. ist ein Treffer
  2. ist kein Treffer
  3. hat ein Schiff versenkt. (Jede dieser Spielausgänge beendet den jeweiligen Spielzug)
Das Spiel endet, wenn einer der beiden Kontrahenten alle Schiffe des Gegners versenkt hat.

Als größte technische Herausforderung sehen wir das benutzerdefinierte setzen der Schiffe.
Werden die Schiffe voher angezeit, sodass sie vom Benutzer nur auf die entsprechenden Felder gesetzt werden müssen?
 Wenn ja, durch drag-and-drop oder einfaches anklicken der Felder auf dem Spielfeld?
 Wenn wir mit BOS als Benutzeroberfläche arbeiten ist drag-and-drop überhaupt möglich?

Ein Lösungsansatz könnte sein, das allein der Benutzer die Schiffe als 1er, 2er, 3er, 4er Schiffe erkennt
und das Programm lediglich mit einzelnen farbigen Feldern arbeitet. Bei zwei 1er/2er/3er und einem 4er Schiff arbeitet das Programm mit 15 farbigen Feldern.
Die Schiffe werden zu Beginn neben dem Spielfeld angezeigt. Beim Klicken auf ein Feld des Spielfeldes, wird dieses gefärbt und gleichzeitg eines der gefärbten
Felder seitlich des Spielfeldes (Darstellung der zu setzenden Schiffe) verschwindet bzw erhält die Hintergrundfarbe.